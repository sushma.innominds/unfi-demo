import Prisma from "@prisma/client";
import auth from "../auth/index.js"
import abs from "../abs/index.js";

const prisma = new Prisma.PrismaClient();

const context = async ({ req }) => {

	if (!req?.body?.query) return

	try {
		const publicFetches = ["fetchElements"];
		const { requestType, requestName } = auth.identifyRequest(req?.body?.query);
		let user = {};
		if (requestType !== "query" || (requestType === "query" && !publicFetches.includes(requestName))) {
			const { authorization } = req.headers;
			const token =
				typeof authorization === "string" && authorization.startsWith("Bearer ")
					? authorization.replace("Bearer ", "")
					: false;
			if (!token) abs.throwError.authentication(`#authentication #token`);
			user = await auth.user(token, requestType);
			if (!user) abs.throwError.authentication(`#authentication #user`);
		}
		return { prisma, ...user};

	} catch (error) {
		abs.throwError.catch(`Context #authentication | message: ${error.message}`);
	}
};
export default context;
