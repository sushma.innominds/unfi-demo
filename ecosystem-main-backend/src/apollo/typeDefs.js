import { gql } from "apollo-server";

const typeDefs = gql`
	# --------- Enum ---------
	enum type {
		text
		picture
	}

	# --------- Element ---------
	type Element {
		id: ID!
		author_id: String!
		isPublic: Boolean
		value: String
		type: String
		created: String
		updated: String
		normalized: Boolean
	}

	# ---------- INPUT --------------

	input id {
		id: ID!
	}

	input newElement {
		isPublic: Boolean
		value: String!
		type: type
	}

	# ---------- QUERY --------------
	type Query {
		fetchElementsPublic(input: id!): [Element]
		fetchElementsPrivate: [Element]
	}

	# ---------- MUTATION -----------
	type Mutation {
		createElement(input: newElement!): Element
	}
`;
export default typeDefs;
