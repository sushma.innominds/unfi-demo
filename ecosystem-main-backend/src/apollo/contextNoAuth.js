import Prisma from "@prisma/client";
import abs from "../abs/index.js";

const prisma = new Prisma.PrismaClient();

const contextNoAuth = async ({ req }) => {
	try {
		return {
			prisma,
			user: {id: "no-auth-default-user-id"},
		};
	} catch (error) {
		abs.throwError.catch(`Context #authentication | message: ${error.message}`);
	}
};
export default contextNoAuth;
