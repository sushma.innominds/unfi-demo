import { DataSource } from "apollo-datasource";
import request from "../requests/index.js";
import abs from "../abs/index.js";

export default class Element extends DataSource {
	constructor() {

		super();

		this.itemName = "element";

		this.request = {
			fetchPublic: request.fetch.elementsPublic,
			fetchPrivate: request.fetch.elementsPrivate,
			create: request.create.element,
		};
	}

	initialize(config) {
		this.context = config.context;
	}

	async fetchPrivate( _, { prisma, user }) {
		console.log('\n\nfetchPrivate user.id > ', user.id)
		try {
			return await this.request.fetchPrivate(user.id, this.itemName, prisma);
		} catch (error) {
			abs.throwError.catch(`#fetchPrivate elements | message: ${error.message}`);
		}
	}

	async fetchPublic({ input }, { prisma }) {
		const { id } = input;
		console.log('\n\nfetchPublic id (of author) > ', id)
		try {
			return await this.request.fetchPublic(id, this.itemName, prisma);
		} catch (error) {
			abs.throwError.catch(`#fetchPublic elements | message: ${error.message}`);
		}
	}

	async create({ input }, { prisma, user }) {
		console.log('\n\ncreate input > \n', JSON.stringify(input, 0, 2))
		try {
			const create = this.request.create(input, user.id, this.itemName, prisma);
			const result = await create;
			return abs.normalize.createResponse(result);
		} catch (error) {
			abs.throwError.catch(`#create element | message: ${error.message}`);
		}
	}
}
