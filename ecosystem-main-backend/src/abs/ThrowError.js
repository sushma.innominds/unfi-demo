import {ApolloError, AuthenticationError, UserInputError} from 'apollo-server-errors';

export default class ThrowError {

    authentication(details) {
        // save the log
        console.error('console.error authentication > ', details)
        throw new AuthenticationError (`#authentication: ${details}`)
    };

    if(details) {
        // save the log
        console.error('console.error if > ', details)
        throw new UserInputError (`#if: ${details}`)
    };

    catch(details) {
        // save the log
        console.error('console.error catch >', details)
        throw new ApolloError (`#catch: ${details}`)
    };

};