import Normalize from "./Normalize.js";
import ThrowError from "./ThrowError.js";

const abs = {
	normalize: new Normalize(),
	throwError: new ThrowError(),
};
export default abs;
