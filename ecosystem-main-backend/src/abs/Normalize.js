export default class Normalize {
	createResponse = (result) => {
		const response = { ...result, normalized: true };
		return response;
	};
}
