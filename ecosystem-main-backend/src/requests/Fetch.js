export default class Fetch {

	elementsPublic(author_id, itemName, prisma) {
		return prisma[itemName].findMany({
			orderBy: [{created: 'desc'}],
			take:10, 
			where: { 
				author_id, 
				isPublic: true 
			},
		});
	}

	elementsPrivate(author_id, itemName, prisma) {
		return prisma[itemName].findMany({
			orderBy: [{created: 'desc'}],
			take:10,
			where: {
				author_id, 
				isPublic: false 
			},
		});
	}

}
