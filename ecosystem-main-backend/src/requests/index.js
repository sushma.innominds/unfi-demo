// import requests
import Fetch from "./Fetch.js";
import Create from "./Create.js";

const request = {
	fetch: new Fetch(),
	create: new Create(),
};
export default request;
