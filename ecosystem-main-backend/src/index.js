import "graphql-import-node";
import { ApolloServer } from "apollo-server";
import typeDefs from "./apollo/typeDefs.js";
import resolvers from "./apollo/resolvers.js";
import dataSources from "./apollo/dataSources.js";
import context from "./apollo/context.js";
//import context from "./apollo/contextNoAuth.js";


const { MAIN_BACKEND_SERVER_PORT, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME, IS_PLAYGROUND_ON} = process.env;

if (DB_USER && DB_PASSWORD && DB_HOST && DB_PORT && DB_NAME) {
	process.env.DATABASE_URL = `postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?connection_limit=5&pool_timeout=2`;
}

const server = new ApolloServer({
	typeDefs,
	resolvers,
	dataSources,
	context,
	playground: IS_PLAYGROUND_ON ? true : false,
});

server.listen({ port: MAIN_BACKEND_SERVER_PORT }).then(({ url }) => {
	console.log(`Apollo Server Listening on url ${url}`);
});
