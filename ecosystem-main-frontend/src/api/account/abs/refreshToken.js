import keycloak from './keycloak.js';

const refreshToken = async () => {

    try {

        // update if validity is less than 10 seconds
        await keycloak.updateToken(10);

        console.log('keycloack.token after refresh > \n', keycloak.token)
        
        return keycloak;
    }
    catch (error) {console.log(error)}
};
export default refreshToken;