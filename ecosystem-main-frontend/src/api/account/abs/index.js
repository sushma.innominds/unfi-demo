import refreshToken from './refreshToken.js';
import keycloak from './keycloak.js';

const abs = {

    refreshToken,
    keycloak,

};

export default abs;