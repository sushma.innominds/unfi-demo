import abs from './abs/index.js';
import recoil from './recoil/index.js';

const account = {

    abs,
    recoil,

    cache: {

        isAuthInitialized: false,
        isAuthenticated: false,

    },

};
export default account;