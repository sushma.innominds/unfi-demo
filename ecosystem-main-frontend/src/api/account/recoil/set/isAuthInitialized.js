import keycloak from '../../abs/keycloak.js';
import api from '../../../index.js';

 
const isAuthInitialized = async () => {

    if (!api.account.cache.isAuthInitialized) {

        try {

            await keycloak.init({
                onLoad: 'check-sso',
                silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html'
            })

            api.account.abs.refreshToken()

            api.account.cache.isAuthInitialized = true;

        } catch (error) {console.log(error)}

    }

    api.account.cache.isAuthenticated = keycloak.authenticated;

    console.log( 
        'keycloak initialized',
        '\napi.account.cache > ', api.account.cache,
        '\nkeycloak > ', keycloak,
        '\nkeycloak.token > ', keycloak?.token
    )

    return api.account.cache.isAuthInitialized;
   
};
export default isAuthInitialized;
