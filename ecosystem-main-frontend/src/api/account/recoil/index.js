import {atomFamily, selectorFamily} from 'recoil';
import set from './set/index.js';

const recoil = {

        isAuthInitialized: atomFamily ({
            key: 'isAuthInitialized',
            default: selectorFamily ({
                key: 'setIsAuthInitialized',
                get: (num) => async () => await set.isAuthInitialized(num)
            }),
        }),

};
export default recoil;
