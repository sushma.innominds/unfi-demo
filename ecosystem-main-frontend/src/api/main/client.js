import {ApolloClient, createHttpLink, InMemoryCache} from '@apollo/client';
import {setContext} from '@apollo/client/link/context/index.js';
import keycloak from '../account/abs/keycloak.js';

const httpLink = createHttpLink({
   uri: process.env.REACT_APP_MAIN_BACKEND_BASE_URL,
});

const authLink = setContext((_, {headers}) => {
  const {token} = keycloak;
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache({}),
  connectToDevTools: true,

  defaultOptions:{
    watchQuery: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all',
    },
  },
});

export default client;
