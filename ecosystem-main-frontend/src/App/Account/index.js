import {useRecoilValue} from 'recoil';
import api from '../../api/index.js';
import keycloak from '../../api/account/abs/keycloak.js'

const Account = () => {

    const isAuthInitialized = useRecoilValue(api.account.recoil.isAuthInitialized('1'));

    const isAuthenticated = api.account.cache.isAuthenticated;

    return (
        <div className='header header-login'>
            {isAuthInitialized 
                ? <div className='block'>
                    {isAuthenticated 
                        ? <button className='action' onClick={() => keycloak.logout()}>CLICK TO LOGOUT</button>
                        : <button className='action' onClick={() => keycloak.login()}>CLICK TO LOGIN</button>
                    }
                    <div className='line'>
                        <div className='key'>user id:</div>
                        <div className='value'>{isAuthenticated ? keycloak?.tokenParsed?.sub : 'user is not authenticated'}</div>
                    </div>
                  </div>
                  
                : null 
            }
        </div>
    )
};
export default Account; 