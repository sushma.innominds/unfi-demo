const Account = () => {

    return (
        <div className="block">
            <div className='action disabled'>
                AUTHENTICATION DISABLED
            </div>
            <div className='line'>
                <div className='key'>user id:</div>
                <div className='value'>no-auth-default-user-id</div>
            </div>
        </div>
    )
};
export default Account; 