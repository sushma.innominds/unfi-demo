import {NavLink} from 'react-router-dom';

const Menu = () => {

    return (
        <div className='menu'>
            <NavLink className={({isActive}) => `menu-item${isActive ? ' active':''}`} to='/' end >
                <span>CREATE</span>
            </NavLink>
            <NavLink className={({isActive}) => `menu-item middle${isActive ? ' active':''}`} to='/fetch' end >
                <span>FETCH</span>
            </NavLink>
            <NavLink className={({isActive}) => `menu-item${isActive ? ' active':''}`} to='/token' end >
                <span>TOKEN</span>
            </NavLink>
        </div>
    )
};
export default Menu;
