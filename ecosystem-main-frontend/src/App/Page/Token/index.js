import {useRecoilValue} from 'recoil';
import api from '../../../api/index.js';
import refreshToken from '../../../api/account/abs/refreshToken.js';
// import keycloak from '../../../api/account/abs/keycloak.js'

const Token = () => {

    const isAuthInitialized = useRecoilValue(api.account.recoil.isAuthInitialized('1'));

    const isAuthenticated = api.account.cache.isAuthenticated;

    const onClickRefreshToken = async () => await refreshToken()

    return (
        <div className='page page-user'>
            {isAuthInitialized 
                ? <div>
                    <br/>
                    <div className='user-token'>
                        {isAuthenticated 
                        ? <>
                            <button className='action' onClick={onClickRefreshToken}>REFRESH TOKEN</button>
                            <div className='line'>
                                <div className='key'>Open the console to see the token</div>
                            </div>
                        </>
                        : <>
                            <button className='action disabled'>REFRESH TOKEN</button>
                            <div className='line'>
                                <div className='key'>User is not logged in</div>
                            </div>
                        </>
                        }
                    </div>
                  </div>
                : null 
            }

        </div>
    )
};
export default Token;