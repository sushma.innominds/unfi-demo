import {useState, useEffect} from 'react';
import {gql} from '@apollo/client';
import refreshToken from '../../../api/account/abs/refreshToken.js';
import client from '../../../api/main/client.js';

// GRAPHQL MUTATION 
const fetchElementsPrivate = gql`
    query fetchElementsPrivate {
        fetchElementsPrivate {
            id
            value
            author_id
            isPublic
            type
            created
            updated    
        }
    }
`;

// GRAPHQL MUTATION 
const fetchElementsPublic = gql`
    query fetchElementsPublic ($input: id!) {
        fetchElementsPublic(input: $input) {
            id
            value
            author_id
            isPublic
            type
            created
            updated
        }       
    }
`;

// AGENT COMPONENT TO RUN THE ASYNC OPEATION INSIDE USE EFFECT
const Agent = ({isFetchPublic, id, setResponse}) => {

    useEffect (()=> {

        const fetchElementsPublicQuery = async () => {

            try {
                await refreshToken();
                const response = await client.query ({
                    query: fetchElementsPublic,  
                    variables: {input: {id}}
                });

                console.log('response > ', response);

                setResponse(response?.data?.fetchElementsPublic || '');

            } catch (error) {setResponse([])};
        };

        const fetchElementsPrivateQuery = async () => {

            try {
                await refreshToken();
                const response = await client.query ({
                    query: fetchElementsPrivate, 
                });
                console.log('response protected > ', response);

                setResponse(response?.data?.fetchElementsPrivate || '');

            } catch (error) {setResponse([])};
        };

        if (isFetchPublic) fetchElementsPublicQuery()
        else fetchElementsPrivateQuery()

    })
    return <div style={{display:'none'}}></div>
};

// FETCH COMPONENT
const Fetch = () => {

    const [authorId, setAuthorId] = useState('');
    const [isAgent, setIsAgent] = useState(false);
    const [response, setResponse] = useState('')
    const [isFetchPublic, setIsFetchPublic] = useState(false);

    const onChange = event => {
        setAuthorId(event.target.value);
    };

    const onFetchPublic = event => {
        event.preventDefault();
        console.log('authorId > ', authorId)
        setIsAgent(true)
        setIsFetchPublic(true)
    };

    const onFetchPrivate = event => {
        event.preventDefault();
        setIsAgent(true)
        setIsFetchPublic(false)
    };

    const clearResponse = () => {
        setResponse('')
        setIsFetchPublic('')
        setAuthorId('')
        setIsAgent('')
    }

    return (
        <div>

            <br/>

            {response 
            ? <button className='action radius disabled'>
                FIND MY PRIVATE ELEMENTS
            </button>
            : <button className='action radius' onClick={onFetchPrivate}>
                FIND MY PRIVATE ELEMENTS
            </button>
            }

            <br/>
            <form className='page page-find'>
                <div className='block'>
                    <div className='line'>
                        <label className='key' htmlFor='findUnprotected'>
                            author_id:
                        </label>
                        {response 
                        ? <div className='value disabled'>{authorId ? authorId : '...'}</div>
                        : <input className='value input' 
                            id='findUnprotected'
                            type='text'
                            value={authorId}
                            onChange={onChange}
                            placeholder='Type an author id'
                        />
                        }
                        
                    </div>
                    {response 
                    ? <button className='action disabled'>
                        FIND PUBLIC ELEMENTS OF THIS AUTHOR-ID
                    </button>
                    : <button className='action' type='submit' onClick={authorId ? onFetchPublic : e => e.preventDefault()}>
                        FIND PUBLIC ELEMENTS OF THIS AUTHOR-ID
                    </button>
                    }

                </div>
            </form>

            <br/>
            {response 
            ? <button className='action radius' onClick={clearResponse}>
                CLEAR RESPONSE
              </button>
            : <button className='action radius disabled'>
                CLEAR RESPONSE
              </button>
            }

            {isAgent && !response ? 
                <Agent 
                    id={authorId} 
                    isFetchPublic={isFetchPublic} 
                    setResponse={setResponse} 
                /> 
                : ''
            }
            <br/>
            {!response ? null : 
                !response.length 
                ? <div className='block'>
                    <div className='line'>
                        <div className='value'>There are no elements...</div>
                    </div>
                </div>
                : response.map(element=> {
                    return (
                        <div key={element.id}>
                            <div className='block'>
                                <div className='line'>
                                    <div className='key'>type:</div>
                                    <div className='value'>{element?.type || '...'}</div>
                                </div>
                                <div className='line'>
                                    <div className='key'>value:</div>
                                    <div className='value'>{element?.value || '...'}</div>
                                </div>
                                <div className='line'>
                                    <div className='key'>isPublic:</div> 
                                    <div className='value'>{element ? element?.isPublic ? 'true' : 'false' : '' || '...'}</div>
                                </div>
                                <div className='line'>
                                    <div className='key'>author_id:</div>
                                    <div className='value'>{element?.author_id || '...'}</div>
                                </div>
                                <div className='line'>
                                    <div className='key'>id:</div>
                                    <div className='value'>{element?.id || '...'}</div>
                                </div>
                                <div className='line'>
                                    <div className='key'>created:</div>
                                    <div className='value'>{element?.created || '...'}</div>
                                </div>
                            </div>
                            <br/>
                        </div>
            )})}
        </div>

    );
}
export default Fetch;
