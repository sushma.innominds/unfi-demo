import {useState, useEffect} from 'react';
import client from '../../../api/main/client.js';
import refreshToken from '../../../api/account/abs/refreshToken.js';
import {gql} from '@apollo/client';

// GRAPHQL MUTATION 
const createElement = gql`
    mutation createElement ($input: newElement!) {
        createElement(input: $input) {
            id
            value
            author_id
            isPublic
            type
            created
            updated
        }       
    }
`;

// AGENT COMPONENT TO RUN THE ASYNC OPERATION INSIDE USE EFFECT
const Agent = ({value, type, isPublic, setResponse}) => {
    useEffect (()=> {
        const mutate = async (value, type, isPublic) => {
            try {
                await refreshToken();
                const response = await client.mutate ({
                    mutation: createElement, 
                    variables: {input: {value, type, isPublic: isPublic === 'true' ? true:false}}
                });
                console.log('response > ', response);
                setResponse(response.data.createElement);
            } catch (error) {

                setResponse({
                    id: 'login and retry',
                    value: 'login and retry',
                    author_id: 'login and retry',
                    isPublic: 'login and retry',
                    type: 'login and retry',
                    created: 'login and retry',
                    updated: 'login and retry',
                });

                return 'error'
            };
        };
        mutate(value, type, isPublic);
    })
    return <div style={{display:'none'}}></div>
};


// CREATE COMPONENT 
const Create = () => {

    const [isAgent, setIsAgent] = useState(false);
    const [response, setResponse] = useState('')

    const [value, setValue] = useState('');
    const onChangeValue = event => {
        setValue(event.target.value);
    };

    const [isPublic, setIsPublic] = useState('');
    const onChangeIsPublic = event => {
        setIsPublic(event.target.value);
    };

    const onSubmit = event => {
        event.preventDefault();
        setIsAgent(true)
    };

    const clearResponse = () => {
        setResponse('');
        setIsAgent(false);
        setValue('');
        // setType('');
        setIsPublic('');
    };

    return (
        <div className='page page-new-text'>
            <form>
                <br/>
                    <div className='block'>
                        <div className='line'>
                            <label className='key' htmlFor='newElementType'>
                                type:
                            </label>
                            <div className='value'>text</div> 
                        </div>
                        <div className='line'>
                            <label className='key' htmlFor='newElementValue'>
                                value:
                            </label>
                            {response 
                            ? <div className='value'>{value}</div> 
                            : <input className='value input' 
                                id='newElementValue'
                                type='text'
                                value={value}
                                onChange={onChangeValue}
                                placeholder='Write something..'
                            />}
                        </div>
                        <div className='line'>
                            <label className='key' htmlFor='newElementIsPublic'>
                                isPublic:
                            </label>
                            {response 
                            ? <div className='value'>{isPublic}</div> 
                            : <input className='value input' 
                                id='newElementIsPublic'
                                type='text'
                                value={isPublic}
                                onChange={onChangeIsPublic}
                                placeholder='true or false'
                            />}
                        </div>
                    <button className={`action${response ? ' disabled' : ''}`} type='submit' onClick={onSubmit}>
                        SUBMIT
                    </button>
                </div>
                {isAgent && !response ? 
                    <Agent 
                        value={value}
                        type='text'
                        isPublic={isPublic} 
                        setResponse={setResponse} 
                    /> 
                    : ''}
            </form>
            <br/>
                <div className='block'>
                    <div className='line'>
                        <div className='key'>type:</div>
                        <div className='value'>{response?.type || '...'}</div>
                    </div>
                    <div className='line'>
                        <div className='key'>value:</div>
                        <div className='value'>{response?.value || '...'}</div>
                    </div>
                    <div className='line'>
                        <div className='key'>isPublic:</div> 
                        <div className='value'>{response ? response?.isPublic ? response.isPublic === 'login and retry' ? 'login and retry' : 'true' : 'false' : '' || '...'}</div>
                    </div>
                    <div className='line'>
                        <div className='key'>author_id:</div>
                        <div className='value'>{response?.author_id || '...'}</div>
                    </div>
                    <div className='line'>
                        <div className='key'>id:</div>
                        <div className='value'>{response?.id || '...'}</div>
                    </div>
                    <div className='line'>
                        <div className='key'>created:</div>
                        <div className='value'>{response?.created || '...'}</div>
                    </div>
                    <button className={`action${response ? '' : ' disabled'}`} onClick={clearResponse}>
                        CLEAR BEFORE SUBMITTING A NEW REQUEST
                    </button>
                </div>
            <br/>
        </div>
        
    );
}
export default Create;