import {Suspense} from "react";
import {Outlet} from "react-router-dom";
import Menu from "../Menu/index.js";
import Account from "../Account/index.js";
//import Account from "../Account/noAuth.js";

const Layout = () => {

    return (
        <div className='layout light'>
            <Suspense fallback={
                <div className="header header-login">
                    <div className='block'>
                        <button className='action'>LOADING LOGIN..</button>
                        <div className='line'>
                            <div className='key'>user id:</div>
                            <div className='value'>Loading login...</div>
                        </div>
                    </div>
                </div>
            }>
                <Account/>
            </Suspense>
            <br/>
            <Menu/>
            <Outlet/>
        </div>
    )
};
export default Layout;
